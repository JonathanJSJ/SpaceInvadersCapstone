using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using Microsoft.VisualBasic.FileIO;

namespace SpaceInvadersCapstone.Models.Utils;

public class FileManager
{
    public static ObservableCollection<Score> GetSavedScores()
    {
        string homeDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
        string targetDirectory = Path.Combine(homeDirectory, "SpaceInvadersCapstone");

        if (!Directory.Exists(targetDirectory))
        {
            return new ObservableCollection<Score>();
        }
        
        string[] files = Directory.GetFiles(targetDirectory, "SavedScores.csv");

        
        if (files.Length > 0)
        {
            return ConvertCsvToUsersList(files[0]);
        }

        return new ObservableCollection<Score>();
    }

    private static ObservableCollection<Score> ConvertCsvToUsersList(string csvFilePath)
    {
        ObservableCollection<Score> scoreList = new ObservableCollection<Score>();

        using (TextFieldParser parser = new TextFieldParser(csvFilePath))
        {
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(",");

            while (!parser.EndOfData)
            {
                string[] fields = parser.ReadFields();

                Score score = new Score(fields[0], int.Parse(fields[1]));
                scoreList.Add(score);
            }
        }

        return scoreList;
    }

    public static void SaveScore(Score newScore)
    {
        string score = $"{newScore.PlayerName},{newScore.Points}";
        
        string homeDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
        string targetDirectory = Path.Combine(homeDirectory, "SpaceInvadersCapstone");
        
        if (!Directory.Exists(targetDirectory))
        {
            Directory.CreateDirectory(targetDirectory);
        }
        
        string csvFilePath = Path.Combine(targetDirectory, "SavedScores.csv");
        
        if (File.Exists(csvFilePath))
        {
            using (StreamWriter writer = File.AppendText(csvFilePath))
            {
                writer.WriteLine(score);
            }
        }
        else
        {
            using (StreamWriter writer = File.CreateText(csvFilePath))
            {
                writer.WriteLine(score);
            }
        }
    }
}