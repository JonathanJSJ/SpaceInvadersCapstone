using System;

namespace SpaceInvadersCapstone.Models.Utils;

public class ScoreArg : EventArgs
{
    public ScoreArg(int score)
    {
        Score = score;
    }

    public int Score { get; set; }
}