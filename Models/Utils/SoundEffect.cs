using System.IO;
using LibVLCSharp.Shared;

namespace SpaceInvadersCapstone.Models;

public class SoundEffect
{
    private MediaPlayer MediaPlayer { get; }
    
    public SoundEffect(string file)
    {
        LibVLC libVlc = new LibVLC();
        string filePath = Path.GetFullPath(file);
        Media media = new Media(libVlc, filePath);
        MediaPlayer = new MediaPlayer(media);
    }
    
    public void PlaySound()
    {
        if (MediaPlayer.State == VLCState.Playing)
        {
            return;
        }
        
        MediaPlayer.Stop();
        MediaPlayer.Play();
    }
    
}