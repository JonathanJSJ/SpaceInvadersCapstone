namespace SpaceInvadersCapstone.Models.Utils;

public class Constants
{
    public const int ScreenWidth = 1080;
    public const int ScreenHeight = 720;
    
    public const string ShotSfx = "../../../Assets/SFX/Shot.wav";
    public const string ShieldHitSfx = "../../../Assets/SFX/ShieldDamage.wav";
    public const string EnemyHitSfx = "../../../Assets/SFX/EnemyHit.wav";
    public const string EnemyShotSfx = "../../../Assets/SFX/EnemyShot.wav";
    public const string PlayerMoveSfx = "../../../Assets/SFX/MoveSound.wav";
    public const string PlayerDamageSfx = "../../../Assets/SFX/PlayerDamage.wav";

    public const string SpecialEnemySprite = "../../../Assets/Sprites/SpecialEnemy.png";
    
    public const string ShooterGridEnemySprite1 = "../../../Assets/Sprites/ShooterGridEnemySprite1.png";
    public const string ShooterGridEnemySprite2 = "../../../Assets/Sprites/ShooterGridEnemySprite2.png";
    public const string MidGridEnemySprite1 = "../../../Assets/Sprites/MidGridEnemySprite1.png";
    public const string MidGridEnemySprite2 = "../../../Assets/Sprites/MidGridEnemySprite2.png";
    public const string TankerGridEnemySprite1 = "../../../Assets/Sprites/TankerGridEnemySprite1.png";
    public const string TankerGridEnemySprite2 = "../../../Assets/Sprites/TankerGridEnemySprite2.png";

    public const string ShieldSprite0 = "../../../Assets/Sprites/ShieldSprite0.png";
    public const string ShieldSprite1 = "../../../Assets/Sprites/ShieldSprite1.png";
    public const string ShieldSprite2 = "../../../Assets/Sprites/ShieldSprite2.png";
    public const string ShieldSprite3 = "../../../Assets/Sprites/ShieldSprite3.png";
    
    public const string PlayerSprite = "../../../Assets/Sprites/Player.png";

    public const int DefaultDelay = 16;
    
    public const int PlayerPositionY = 650;
    
    public const int GridEnemiesLeftMargin = 275;
    public const int LeftMarginBetweenGridEnemies = 20;
    public const int GridEnemiesTopMargin = 80;
    public const int TopMarginBetweenGridEnemies = 10;
    public const int GridEnemiesWidth = 40;
    public const int GridEnemiesHeight = 40;

    public const int ShieldsTopMargin = 560;
    public const int ShieldsLeftMargin = 215;
}