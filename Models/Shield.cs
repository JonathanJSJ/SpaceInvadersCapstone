using System;
using Avalonia;
using Avalonia.Controls;
using ReactiveUI;
using SpaceInvadersCapstone.Models.Enemies;
using SpaceInvadersCapstone.Models.Utils;

namespace SpaceInvadersCapstone.Models;

public class Shield : ReactiveObject
{
    private int _life;
    private bool _alive;
    private Image _sprite;
    private Image[] _spritesSequence;
    private int _spriteIndex;
    private SoundEffect _hitSfx;
    
    public Shield(Image[] spritesSequence, Rect hitBox)
    {
        _spritesSequence = spritesSequence;
        _spriteIndex = 0;
        _life = spritesSequence.Length * 3;
        _hitSfx = new SoundEffect(Constants.ShieldHitSfx);
        
        Sprite = spritesSequence[0];
        Alive = true;
        HitBox = hitBox;
    }

    public Rect HitBox { get; }
    
    public Image Sprite
    {
        get => _sprite;
        set => this.RaiseAndSetIfChanged(ref _sprite, value);
    }

    public bool Alive
    {
        get => _alive;
        set => this.RaiseAndSetIfChanged(ref _alive, value);
    }

    private void RegisterDamage()
    {
        if(!Alive){ return;}
        
        --_life;
        
        if (_life==0)
        {
            Alive = false;
        }else if (_life % 3 == 0)
        {
            Sprite = _spritesSequence[++_spriteIndex];
        }
    }

    public delegate void ShieldDamageHandler(object source, object e);
    public event ShieldDamageHandler TakeDamage;

    public void CheckHit(object sender, EventArgs e)
    {
        if (!Alive)
        {
            return;
        }
        if (sender is SpecialEnemy)
        {
            if (HitBox.Intersects(((SpecialEnemy)sender).EnemyShotHitBox))
            {
                _hitSfx.PlaySound();
                RegisterDamage();
                TakeDamage.Invoke(this, (IShooterEnemy)sender);
            }
        }else if (sender is ShooterGridEnemy)
        {
            if (HitBox.Intersects(((ShooterGridEnemy)sender).ShotHitBox))
            {
                _hitSfx.PlaySound();
                RegisterDamage();
                TakeDamage.Invoke(this, (IShooterEnemy)sender);
            }
        }else if (sender is Player)
        {
            if (HitBox.Intersects(((Player)sender).ShotHitBox))
            {
                _hitSfx.PlaySound();
                RegisterDamage();
                TakeDamage.Invoke(this, (Player)sender);
            }
        }
    }
}