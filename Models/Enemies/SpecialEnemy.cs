using System;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using ReactiveUI;
using SpaceInvadersCapstone.Models.Utils;

namespace SpaceInvadersCapstone.Models.Enemies;

public class SpecialEnemy : AbstractEnemy, IShooterEnemy
{
    public bool GameRunning { get; set; }
    private SoundEffect _shotSfx;
    private Rect _enemyShotHitBox;
    private bool _shotVisible;
    
    public Rect EnemyShotHitBox
    {
        get => _enemyShotHitBox;
        set { this.RaiseAndSetIfChanged(ref _enemyShotHitBox, value); }
    }

    public bool ShotVisible
    {
        get => _shotVisible;
        set { this.RaiseAndSetIfChanged(ref _shotVisible, value); }
    }
    
    private bool ShotHitObject { get; set; }


    public SpecialEnemy(Image sprite, Rect hitBox, SoundEffect shotSfx, SoundEffect hitSfx, int points) : 
        base(sprite, hitBox, hitSfx, points)
    {
        _shotSfx = shotSfx;
        EnemyShotHitBox = new Rect(HitBox.X, HitBox.Y +HitBox.Height, 5, 15);
    }
    
    public event EventHandler ShotMove;
    
    public async void StartMovementLoop()
    {
        Random random = new Random();
        int speed = 3;
        int direction = 1;

        while (Alive && GameRunning)
        {
            if (HitBox.X  >= Constants.ScreenWidth || HitBox.X + HitBox.Width <= 0)
            {
                direction *= -1;
                
                int delay = random.Next(10, 15) * 1000;
                await Task.Delay(delay);
            }

            HitBox = new Rect( HitBox.X + speed * direction, HitBox.Y, HitBox.Width, HitBox.Height);

            bool shot = (random.Next(100) == 1);
            if (shot)
            {
                Shoot();
            }

            await Task.Delay(Constants.DefaultDelay);
        }
    }
    
    public async Task Shoot()
    {
        if (!EnemyShotHitBox.Y.Equals(HitBox.Y + HitBox.Height))
        {
            return;
        }
        
        _shotSfx.PlaySound();

        double enemyPositionX = HitBox.X + HitBox.Width / 2;
        double shotY = EnemyShotHitBox.Y;
        int speed = 12;
        ShotVisible = true;

        while (shotY + EnemyShotHitBox.Height <= Constants.ScreenHeight && !ShotHitObject)
        {
            shotY += speed;

            var newPosition = new Rect(enemyPositionX, shotY, EnemyShotHitBox.Width, EnemyShotHitBox.Height);
            EnemyShotHitBox = newPosition;

            if (shotY >450)
            {
                ShotMove.Invoke(this, EventArgs.Empty);
            }

            await Task.Delay(Constants.DefaultDelay);
        }

        var restorePosition = new Rect(EnemyShotHitBox.X, HitBox.Y + HitBox.Height, EnemyShotHitBox.Width, EnemyShotHitBox.Height);
        EnemyShotHitBox = restorePosition;
        ShotVisible = false;
        ShotHitObject = false;
    }
    
    public void OnShieldTakeDamage(object sender, object e)
    {
        if (e.Equals(this))
        {
            ShotHitObject = true;
        }
    }
    
    public void OnPlayerTakeDamage(object sender, IShooterEnemy e)
    {
        if (e.Equals(this))
        {
            ShotHitObject = true;
        }
    }

}