using System;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using ReactiveUI;
using SpaceInvadersCapstone.Models.Utils;

namespace SpaceInvadersCapstone.Models.Enemies;

public class ShooterGridEnemy: GridEnemy, IShooterEnemy
{

    private SoundEffect _shotSfx;
    
    private Rect _shotHitBox;
    public Rect ShotHitBox
    {
        get => _shotHitBox;
        set => this.RaiseAndSetIfChanged(ref _shotHitBox, value);
    }

    private bool _shotVisible;

    public bool ShotVisible
    {
        get => _shotVisible;
        set => this.RaiseAndSetIfChanged(ref _shotVisible, value);
    }
    
    private bool ShotHitObject { get; set; }


    public ShooterGridEnemy(Image[] sprites, Rect hitBox, SoundEffect hitSfx, int points, int row, int col, SoundEffect shotSfx) : 
        base(sprites, hitBox, hitSfx, points, row, col)
    {
        _shotSfx = shotSfx;
        ShotHitBox = new Rect(HitBox.X, HitBox.Y + HitBox.Height, 5, 15);
    }
    
    public async Task Shoot()
    {
        Random random = new Random();
        await Task.Delay(random.Next( 2000));

        if (!Alive)
        {
            return;
        }
        
        double enemyPositionX = HitBox.X + HitBox.Width / 2;
        double shotY = HitBox.Y+HitBox.Height;
        int speed = 5;
        ShotVisible = true;
        _shotSfx.PlaySound();
        
        while (shotY + ShotHitBox.Height <= Constants.ScreenHeight && !ShotHitObject)
        {
            shotY += speed;

            var newPosition = new Rect(enemyPositionX, shotY, ShotHitBox.Width, ShotHitBox.Height);
            ShotHitBox = newPosition;
            
            if (shotY >450)
            {
                ShotMove.Invoke(this, EventArgs.Empty);
            }

            await Task.Delay(Constants.DefaultDelay);
        }
        
        ShotVisible = false;
        ShotHitObject = false;
    }
    

    public event EventHandler ShotMove;

    public void OnShieldTakeDamage(object sender, object e)
    {
        if (e.Equals(this))
        {
            ShotHitObject = true;
        }
    }
    
    public void OnPlayerTakeDamage(object sender, IShooterEnemy e)
    {
        if (e.Equals(this))
        {
            ShotHitObject = true;
        }
    }

}