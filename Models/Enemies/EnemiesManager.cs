using System;
using System.Collections.ObjectModel;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Media.Imaging;
using ReactiveUI;
using SpaceInvadersCapstone.Models.Utils;

namespace SpaceInvadersCapstone.Models.Enemies;

public class EnemiesManager: ReactiveObject
{
    private int _round;
    private int Round
    {
        get => _round;
        set
        {
            if (value >= 6)
            {
                Round = 6;
            }
            else
            {
                _round = value;
            }
        }
    }

    public GridEnemiesManager GridEnemiesManager { get; set; }

    private ObservableCollection<GridEnemy> _gridEnemies;

    public ObservableCollection<GridEnemy> GridEnemies
    {
        get => _gridEnemies;
        set => this.RaiseAndSetIfChanged(ref _gridEnemies, value);
    }

    private SpecialEnemy _specialEnemy;

    public SpecialEnemy SpecialEnemy
    {
        get => _specialEnemy;
        set => this.RaiseAndSetIfChanged(ref _specialEnemy, value);
    }
    

    public EnemiesManager(int round)
    {
        Round = round;
        CreateEnemies();
    }
    

    private void CreateEnemies()
    {
        CreateGridEnemies();
        CreateSpecialEnemy();
        
        GridEnemiesManager = new GridEnemiesManager(GridEnemies);
    }

    private void CreateGridEnemies()
    {
        ObservableCollection<GridEnemy> newGridEnemies = new ();

        Image tankerGridEnemyImage1 = new Image();
        tankerGridEnemyImage1.Source = new Bitmap(Constants.TankerGridEnemySprite1);
        Image tankerGridEnemyImage2 = new Image();
        tankerGridEnemyImage2.Source = new Bitmap(Constants.TankerGridEnemySprite2);
        Image[] tankerImages = new[] { tankerGridEnemyImage1, tankerGridEnemyImage2 };
        
        Image midGridEnemyImage1 = new Image();
        midGridEnemyImage1.Source = new Bitmap(Constants.MidGridEnemySprite1);
        Image midGridEnemyImage2 = new Image();
        midGridEnemyImage2.Source = new Bitmap(Constants.MidGridEnemySprite2);
        Image[] midImages = new[] { midGridEnemyImage1, midGridEnemyImage2 };
        
        Image shooterGridEnemyImage1 = new Image();
        shooterGridEnemyImage1.Source = new Bitmap(Constants.ShooterGridEnemySprite1);
        Image shooterGridEnemyImage2 = new Image();
        shooterGridEnemyImage2.Source = new Bitmap(Constants.ShooterGridEnemySprite2);
        Image[] shooterImages = new[] { shooterGridEnemyImage1, shooterGridEnemyImage2 };

        SoundEffect hitSfx = new SoundEffect(Constants.EnemyHitSfx);
        SoundEffect shotSfx = new SoundEffect(Constants.EnemyShotSfx);

        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                GridEnemy newEnemy;
                Image[] images;
                Rect rect = new Rect(
                    Constants.GridEnemiesLeftMargin + (Constants.LeftMarginBetweenGridEnemies+Constants.GridEnemiesWidth) * j, 
                    Constants.GridEnemiesTopMargin + (Constants.TopMarginBetweenGridEnemies+Constants.GridEnemiesHeight) * i + (40 * Round), 
                    Constants.GridEnemiesWidth,
                    Constants.GridEnemiesHeight);
                
                switch (i)
                {
                    case 0:
                        images = shooterImages;
                        newEnemy = new ShooterGridEnemy(images, rect, hitSfx, 40, i, j, shotSfx);
                        break;
                    case 1:
                    case 2:
                        images = midImages;
                        newEnemy = new GridEnemy(images, rect, hitSfx, 20, i, j);
                        break;
                    default:
                        images = tankerImages;
                        newEnemy = new GridEnemy(images, rect, hitSfx, 10, i, j);
                        break;
                }
                
                newGridEnemies.Add(newEnemy);
            }
        }
        GridEnemies = newGridEnemies;
    }

    private void CreateSpecialEnemy()
    {
        Image specialEnemyImage = new Image();
        specialEnemyImage.Source = new Bitmap(Constants.SpecialEnemySprite);
        
        Rect specialEnemyHitbox = new Rect(Constants.ScreenWidth, 30, 60, 40);

        Random random = new Random();
        int randomValue = random.Next(1, 5);
        
        SpecialEnemy = new SpecialEnemy(
            specialEnemyImage, 
            specialEnemyHitbox, 
            new SoundEffect(Constants.EnemyShotSfx),
            new SoundEffect(Constants.EnemyHitSfx),
            100 * randomValue);
    }
    
    public void StartEnemies()
    {
        GridEnemiesManager.GameRunning = true;
        SpecialEnemy.GameRunning = true;
        GridEnemiesManager.StartMovementLoop();
        GridEnemiesManager.StartShootLoop();
        SpecialEnemy.StartMovementLoop();
    }

    public void OnEndGame(object sender, EventArgs e)
    {
        GridEnemiesManager.GameRunning = false;
        SpecialEnemy.GameRunning = false;
    }
}