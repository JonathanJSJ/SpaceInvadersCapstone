using System;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using ReactiveUI;

namespace SpaceInvadersCapstone.Models.Enemies;

public abstract class AbstractEnemy : ReactiveObject
{
    private Image _sprite;

    public Image Sprite
    {
        get => _sprite;
        set => this.RaiseAndSetIfChanged(ref _sprite, value);
    }

    private Rect _hitBox;
    public Rect HitBox
    {
        get => _hitBox;
        set => this.RaiseAndSetIfChanged(ref _hitBox, value);
    }
    
    private bool _alive;

    public bool Alive
    {
        get => _alive;
        set => this.RaiseAndSetIfChanged(ref _alive, value);
    }

    private readonly SoundEffect _hitSfx;
    
    public int Points { get; }

    protected AbstractEnemy(Image sprite, Rect hitBox, SoundEffect hitSfx, int points)
    {
        Sprite = sprite;
        HitBox = hitBox;
        Alive = true;
        _hitSfx = hitSfx;
        Points = points;
    }

    public event EventHandler TakeDamage;

    public void OnPlayerShotMove(object sender, EventArgs e)
    {
        if (!Alive)
        {
            return;
        }

        if (HitBox.Intersects((sender as Player).ShotHitBox))
        {
            _hitSfx.PlaySound();
            Alive = false;
            
            TakeDamage.Invoke(this, EventArgs.Empty);
        }
    }
}