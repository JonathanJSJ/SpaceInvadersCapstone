using System;
using System.Threading.Tasks;
using SpaceInvadersCapstone.Models.Utils;

namespace SpaceInvadersCapstone.Models.Enemies;

public interface IShooterEnemy
{
    Task Shoot();

    event EventHandler ShotMove;

    void OnPlayerTakeDamage(object sender, IShooterEnemy e);

    void OnShieldTakeDamage(object sender, object e);
}