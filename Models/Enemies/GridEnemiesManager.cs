using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Avalonia;
using ReactiveUI;
using SpaceInvadersCapstone.Models.Utils;

namespace SpaceInvadersCapstone.Models.Enemies;

public class GridEnemiesManager : ReactiveObject
{
    public bool GameRunning { get; set; }

    private double _speed;
    
    private ObservableCollection<GridEnemy> _enemies;
    
    private int _enemiesAlive;

    private int _pivotEnemyIndex;
    
    private int [] _numberOfEnemiesPerRows;

    private int [] _numberOfEnemiesPerCol;

    private Rect _enemiesGridHitBox;
    
    public Rect EnemiesGridHitBox
    {
        get => _enemiesGridHitBox;
        set => this.RaiseAndSetIfChanged(ref _enemiesGridHitBox, value);
    }
    
    private ShooterGridEnemy _shooter1;

    public ShooterGridEnemy Shooter1
    {
        get => _shooter1;
        set => this.RaiseAndSetIfChanged(ref _shooter1, value);
    }
    
    private ShooterGridEnemy _shooter2;

    public ShooterGridEnemy Shooter2
    {
        get => _shooter2;
        set => this.RaiseAndSetIfChanged(ref _shooter2, value);
    }
    
    private ShooterGridEnemy _shooter3;

    public ShooterGridEnemy Shooter3
    {
        get => _shooter3;
        set => this.RaiseAndSetIfChanged(ref _shooter3, value);
    }

    public GridEnemiesManager(ObservableCollection<GridEnemy> enemies)
    {
        _enemies = enemies;
        _enemiesAlive = 50;
        _speed = 1;
        _pivotEnemyIndex = 0;
        _numberOfEnemiesPerRows = new [] { 10, 10, 10, 10, 10 };
        _numberOfEnemiesPerCol = new [] { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5};
        EnemiesGridHitBox = new Rect(enemies[0].HitBox.X, enemies[0].HitBox.Y, 580, 250);
    }

    public async void StartMovementLoop()
    {
        int direction = 1;
        
        while (_numberOfEnemiesPerRows[0] > 0 && GameRunning)
        {
            if (_enemies[_pivotEnemyIndex].HitBox.X + EnemiesGridHitBox.Width >= Constants.ScreenWidth || _enemies[_pivotEnemyIndex].HitBox.X <= 0)
            {                                           
                direction *= -1;   
                _speed += 0.4;

                EnemiesGridHitBox = new Rect(EnemiesGridHitBox.X, EnemiesGridHitBox.Y +20, EnemiesGridHitBox.Width, EnemiesGridHitBox.Height);
                
                foreach (var gridEnemy in _enemies)
                {
                    gridEnemy.Move(_speed, 0);
                }
            }

            EnemiesGridHitBox = new Rect( EnemiesGridHitBox.X + _speed * direction, EnemiesGridHitBox.Y, EnemiesGridHitBox.Width, EnemiesGridHitBox.Height);
            
            foreach (var gridEnemy in _enemies)
            {
                gridEnemy.Move(_speed, direction);
            }

            if (EnemiesGridHitBox.Y + EnemiesGridHitBox.Height >= Constants.PlayerPositionY)
            {
                EnemyReachPlayer.Invoke(this, EventArgs.Empty);
                break;
            }
            
            await Task.Delay(Constants.DefaultDelay);
        }
    }
    
    public event EventHandler EnemyReachPlayer;


    public async void StartShootLoop()
    {
        Random random = new Random();
        List<Task> tasks;

        while (_numberOfEnemiesPerRows[0] > 0 && GameRunning)
        {
            int delay = Convert.ToInt32(2000 - (300 * _speed));
            if (delay > 0)
            {
                await Task.Delay(delay);
            }
            
            tasks = new List<Task>();
            
            int chosenEnemy1;
            
            do
            {
                chosenEnemy1 = random.Next(10);
            } while (!_enemies[chosenEnemy1].Alive);

            Shooter1 = (ShooterGridEnemy)_enemies[chosenEnemy1];
            Task shoot1 = Shooter1.Shoot();
            tasks.Add(shoot1);
            
            if (_numberOfEnemiesPerRows[0] > 1)
            {
                int chosenEnemy2;
                
                do
                {
                    chosenEnemy2 = random.Next(10); 

                } while (chosenEnemy2 == chosenEnemy1 || !_enemies[chosenEnemy2].Alive);
                
                Shooter2 = (ShooterGridEnemy)_enemies[chosenEnemy2];
                Task shoot2 = Shooter2.Shoot();
                tasks.Add(shoot2);
                
                if (_numberOfEnemiesPerRows[0] > 2)
                {
                    int chosenEnemy3;
                    
                    do
                    {
                        chosenEnemy3 = random.Next(10);
                    } while (chosenEnemy3 == chosenEnemy1 || chosenEnemy3 == chosenEnemy2 || !_enemies[chosenEnemy3].Alive);
                    
                    Shooter3 = (ShooterGridEnemy)_enemies[chosenEnemy3];
                    Task shoot3 = Shooter3.Shoot();
                    tasks.Add(shoot3);
                }
            }
            await Task.WhenAll(tasks);
        }
    }

    public event EventHandler AllEnemiesEliminated;
    
    public void OnEnemyHit(object subject, EventArgs e)
    {
        _numberOfEnemiesPerRows[((GridEnemy) subject).Row]--;
        _numberOfEnemiesPerCol[((GridEnemy)subject).Col]--;

        _enemiesAlive--;
        if (_enemiesAlive == 0)
        {
            AllEnemiesEliminated.Invoke(this, EventArgs.Empty);
        }
        
        CheckEmptyRow();
        CheckEmptyCol();
    }
    
    private void CheckEmptyRow()
    {
        for (int i = 4; i >= 0; i--)
        {
            if (_numberOfEnemiesPerRows[i]>0)
            {
                return;
            }
            if (_numberOfEnemiesPerRows[i] == 0)
            {
                _numberOfEnemiesPerRows[i]--;

                EnemiesGridHitBox = new Rect(EnemiesGridHitBox.X, EnemiesGridHitBox.Y, EnemiesGridHitBox.Width, EnemiesGridHitBox.Height - 50);
            }
        }
    }

    private void CheckEmptyCol()
    {
        for (int i = 0; i < 10; i++)
        {
            if (_numberOfEnemiesPerCol[i]>0)
            {
                break;
            }
            if (_numberOfEnemiesPerCol[i] == 0)
            {
                _numberOfEnemiesPerCol[i]--;

                _pivotEnemyIndex++;
                EnemiesGridHitBox = new Rect(EnemiesGridHitBox.X, EnemiesGridHitBox.Y, EnemiesGridHitBox.Width - (Constants.GridEnemiesWidth + Constants.LeftMarginBetweenGridEnemies), EnemiesGridHitBox.Height);
            }
        }
        
        for (int i = 9; i >= 0; i--)
        {
            if (_numberOfEnemiesPerCol[i]>0)
            {
                return;
            }
            if (_numberOfEnemiesPerCol[i] == 0)
            {
                _numberOfEnemiesPerCol[i]--;

                EnemiesGridHitBox = new Rect(EnemiesGridHitBox.X, EnemiesGridHitBox.Y, EnemiesGridHitBox.Width - (Constants.GridEnemiesWidth + Constants.LeftMarginBetweenGridEnemies), EnemiesGridHitBox.Height);
            }
        }
    }

}