using Avalonia;
using Avalonia.Controls;

namespace SpaceInvadersCapstone.Models.Enemies;

public class GridEnemy : AbstractEnemy
{
    private int _animationDelay;
    
    private int _spriteIndex;

    private Image[] _spriteSequence;
    public GridEnemy(Image[] sprites, Rect hitBox, SoundEffect hitSfx, int points, int row, int col) : base(sprites[0], hitBox, hitSfx, points)
    {
        _spriteSequence = sprites;
        _spriteIndex = 0;
        _animationDelay = 0;
        Row = row;
        Col = col;
    }
    
   public int Row { get; }

   public int Col { get; }

   public void Move(double speed, int direction)
   {
       if (direction == 1 || direction == -1)
       {
           HitBox = new Rect(
               HitBox.X + speed * direction,
               HitBox.Y,
               HitBox.Width,
               HitBox.Height);

           _animationDelay++;

           if (_animationDelay == 16)
           {
               _spriteIndex++;
           
               if (_spriteIndex == _spriteSequence.Length)
               {
                   _spriteIndex = 0;
               }

               Sprite = _spriteSequence[_spriteIndex];

               _animationDelay = 0;
           }
       }
       else
       {
           HitBox = new Rect(
               HitBox.X,
               HitBox.Y + 20,
               HitBox.Width,
               HitBox.Height);
       }
       
   }
    
}