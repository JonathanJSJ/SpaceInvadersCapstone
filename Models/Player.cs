using System;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using ReactiveUI;
using SpaceInvadersCapstone.Models.Enemies;
using SpaceInvadersCapstone.Models.Utils;

namespace SpaceInvadersCapstone.Models;

public class Player : ReactiveObject
{
    private readonly SoundEffect _shotSfx;

    private readonly SoundEffect _moveSfx;
    
    private readonly SoundEffect _damageSfx;

    private bool _shotHit;

    private int _life;
    public Image Sprite { get; }
    
    private Rect _hitBox;
    
    public Rect HitBox
    {
        get => _hitBox;
        set { this.RaiseAndSetIfChanged(ref _hitBox, value); }
    }

    private Rect _shotHitBox;

    public Rect ShotHitBox
    {
        get => _shotHitBox;
        set { this.RaiseAndSetIfChanged(ref _shotHitBox, value); }
    }

    private bool _shotVisible;

    public bool ShotVisible
    {
        get => _shotVisible;
        set { this.RaiseAndSetIfChanged(ref _shotVisible, value); }
    }

    public int Lives
    {
        get => _life;
        set => this.RaiseAndSetIfChanged(ref _life, value);
    }

    public delegate void PlayerDamageHandler(object source, IShooterEnemy e);
    public event PlayerDamageHandler TakeDamage;
    public event EventHandler ShotMove;
    public event EventHandler PlayerDead;

    
    public Player(Image sprite, Rect hitBox)
    {
        Sprite = sprite;
        HitBox = hitBox;
        _shotSfx = new SoundEffect(Constants.ShotSfx);
        _moveSfx = new SoundEffect(Constants.PlayerMoveSfx);
        _damageSfx = new SoundEffect(Constants.PlayerDamageSfx);
        ShotHitBox = new Rect(HitBox.X, Constants.PlayerPositionY, 5, 15);;
        ShotVisible = false;
        Lives = 3;
    }

    public async void Shot()
    {
        if (!ShotHitBox.Y.Equals(HitBox.Y))
        {
            return;
        }
        
        _shotSfx.PlaySound();

        double playerPositionX = HitBox.X + 20;
        double shotY = ShotHitBox.Y;
        const int speed = 10;
        ShotVisible = true;

        while (shotY > 0 && !_shotHit)
        {
            shotY -= speed;

            var newPosition = new Rect(playerPositionX, shotY, ShotHitBox.Width, ShotHitBox.Height);
            ShotHitBox = newPosition;

            ShotMove.Invoke(this, EventArgs.Empty);

            await Task.Delay(12);
        }

        var restorePosition = new Rect(ShotHitBox.X, HitBox.Y, ShotHitBox.Width, ShotHitBox.Height);
        ShotHitBox = restorePosition;
        ShotVisible = false;
        _shotHit = false;
    }
    
    public enum Directions
    {
        Left = -1,
        Right = 1
    }

    public void Move(Directions direction)
    {
        int displacement = 10 * (int) direction;

        if (HitBox.Width + HitBox.X + displacement > Constants.ScreenWidth ||
            HitBox.X + displacement < 0)
        {
            return;
        }

        _moveSfx.PlaySound();
        var newRect = new Rect(HitBox.X + displacement, HitBox.Y, HitBox.Width, HitBox.Height);
        HitBox = newRect;
    }

    public void CheckHit(object sender, EventArgs e)
    {
        if (sender is SpecialEnemy)
        {
            if (HitBox.Intersects(((SpecialEnemy)sender).EnemyShotHitBox))
            {
                Lives--;
                _damageSfx.PlaySound();
                TakeDamage.Invoke(this, (IShooterEnemy)sender);
            }
        }else if (sender is ShooterGridEnemy)
        {
            if (HitBox.Intersects(((ShooterGridEnemy)sender).ShotHitBox))
            {
                Lives--;
                _damageSfx.PlaySound();
                TakeDamage.Invoke(this, (IShooterEnemy)sender);
            }
        }

        if (Lives == 0)
        {
            PlayerDead.Invoke(this, EventArgs.Empty);
        }
    }
    
    public void OnShotHitEnemy(object sender, EventArgs e)
    {
        _shotHit = true;
    }

    public void OnShieldTakeDamage(object sender, object e)
    {
        if (e.Equals(this))
        {
            _shotHit = true;
        }
    }
}