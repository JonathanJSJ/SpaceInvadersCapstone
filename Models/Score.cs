namespace SpaceInvadersCapstone.Models;

public class Score
{
    public string PlayerName { get; set; }
    public int Points { get; set; }

    public Score(string playerName, int points)
    {
        PlayerName = playerName;
        Points = points;
    }

    public Score(int points)
    {
        Points = points;
    }
}