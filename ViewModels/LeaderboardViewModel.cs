using System;
using System.Collections.ObjectModel;
using System.Reactive.Linq;
using ReactiveUI;
using SpaceInvadersCapstone.Models;
using SpaceInvadersCapstone.Models.Utils;

namespace SpaceInvadersCapstone.ViewModels;

public class LeaderboardViewModel : ViewModelBase
{
    private ObservableCollection<Score> _scores;

    public ObservableCollection<Score> Scores
    {
        get => _scores;
        set => this.RaiseAndSetIfChanged(ref _scores, value);
    }
    
    public void OnExitButtonClicked()
    {
        ExitToMainScreen.Invoke(this, EventArgs.Empty);
    }

    public event EventHandler ExitToMainScreen;

    public void UpdateList()
    {
        Scores = FileManager.GetSavedScores();
    }
}