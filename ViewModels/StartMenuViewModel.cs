using System;

namespace SpaceInvadersCapstone.ViewModels;

public class StartMenuViewModel : ViewModelBase
{
    public void OnStartButtonClick()
    {
        StartGameOption.Invoke(this, EventArgs.Empty);
    }
    public event EventHandler StartGameOption;
    
    public void OnLeaderboardButtonClick()
    {
        LeaderboardOption.Invoke(this, EventArgs.Empty);
    }
    public event EventHandler LeaderboardOption;

    public void OnGameControlsButtonClick()
    {
        GameControlsOption.Invoke(this, EventArgs.Empty);
    }

    public event EventHandler GameControlsOption;
}