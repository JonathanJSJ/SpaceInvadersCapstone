using System;
using System.Collections.ObjectModel;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Media.Imaging;
using ReactiveUI;
using SpaceInvadersCapstone.Models;
using SpaceInvadersCapstone.Models.Enemies;
using SpaceInvadersCapstone.Models.Utils;

namespace SpaceInvadersCapstone.ViewModels;

public class GameViewModel : ViewModelBase
{
    private ObservableCollection<Shield>? _shields;

    public ObservableCollection<Shield>? Shields
    {
        get => _shields;
        set => this.RaiseAndSetIfChanged(ref _shields, value);
    }

    private ObservableCollection<GridEnemy>? _gridEnemies;

    public ObservableCollection<GridEnemy>? GridEnemies
    {
        get => _gridEnemies;
        set => this.RaiseAndSetIfChanged(ref _gridEnemies, value);
    }

    private SpecialEnemy _specialEnemy;

    public SpecialEnemy SpecialEnemy
    {
        get => _specialEnemy;
        set => this.RaiseAndSetIfChanged(ref _specialEnemy, value);
    }
    
    private int _score;

    public int Score
    {
        get => _score;
        set => this.RaiseAndSetIfChanged(ref _score, value);
    }

    private EnemiesManager _enemiesManager;

    public EnemiesManager EnemiesManager
    {
        get => _enemiesManager;
        set => this.RaiseAndSetIfChanged(ref _enemiesManager, value);
    }

    public Player _player;

    public Player Player
    {
        get => _player;
        private set => this.RaiseAndSetIfChanged(ref _player, value);
    }
    
    private int _round;
    
    private int _extraLifeIndex;
    
    private void StartNewGame()
    {
        Score = 0;
        _round = 0;
        _extraLifeIndex = 1000;
        CreatePlayer();
        CreateShields();
        CreateEnemies();
        ConfigSubscribers();
        EnemiesManager.StartEnemies();
    }

    private void CreatePlayer()
    {
        Image sprite = new Image();
        sprite.Source = new Bitmap(Constants.PlayerSprite);
        Rect playerHitBox = new Rect(500, Constants.PlayerPositionY, 50, 50);
        
        Player = new Player(sprite, playerHitBox);
    }
    
    private void CreateShields()
    {
        Image[] spriteSequence = new Image[4];

        Image image0 = new Image();
        image0.Source = new Bitmap(Constants.ShieldSprite0);
        spriteSequence[0] = image0;
        Image image1 = new Image();
        image1.Source = new Bitmap(Constants.ShieldSprite1);
        spriteSequence[1] = image1;
        Image image2 = new Image();
        image2.Source = new Bitmap(Constants.ShieldSprite2);
        spriteSequence[2] = image2;
        Image image3 = new Image();
        image3.Source = new Bitmap(Constants.ShieldSprite3);
        spriteSequence[3] = image3;

        ObservableCollection<Shield> shields = new ObservableCollection<Shield>();

        for (int i = 0; i < 4; i++)
        {
            Rect rect = new Rect(Constants.ShieldsLeftMargin + (180 * i), Constants.ShieldsTopMargin, 80, 50);
            Shield newShield = new Shield(spriteSequence, rect);
            shields.Add(newShield);
        }

        Shields = shields;
    }

    private void CreateEnemies()
    {
        EnemiesManager = new EnemiesManager(_round);
        GridEnemies = EnemiesManager.GridEnemies;
        SpecialEnemy = EnemiesManager.SpecialEnemy;
    }
    
    private void ConfigSubscribers()
    {
        foreach (var shield in Shields)
        {
            Player.ShotMove += shield.CheckHit;
            SpecialEnemy.ShotMove += shield.CheckHit;
            shield.TakeDamage += Player.OnShieldTakeDamage;
            shield.TakeDamage += SpecialEnemy.OnShieldTakeDamage;
        }

        foreach (var enemy in GridEnemies)
        {
            Player.ShotMove += enemy.OnPlayerShotMove;
            enemy.TakeDamage += Player.OnShotHitEnemy;
            enemy.TakeDamage += EnemiesManager.GridEnemiesManager.OnEnemyHit;
            enemy.TakeDamage += OnEnemyTakeDamage;

            if (enemy is ShooterGridEnemy)
            {
                ShooterGridEnemy shooter = ((ShooterGridEnemy)enemy);
                shooter.ShotMove += Player.CheckHit;
                Player.TakeDamage += shooter.OnPlayerTakeDamage;

                foreach (var shield in Shields)
                {
                    shooter.ShotMove += shield.CheckHit;
                    shield.TakeDamage += shooter.OnShieldTakeDamage;
                }
            }
        }

        Player.ShotMove += SpecialEnemy.OnPlayerShotMove;
        Player.TakeDamage += SpecialEnemy.OnPlayerTakeDamage;
        Player.PlayerDead += OnPlayerDeath;
        SpecialEnemy.ShotMove += Player.CheckHit;
        SpecialEnemy.TakeDamage += Player.OnShotHitEnemy;
        SpecialEnemy.TakeDamage += OnEnemyTakeDamage;
        EnemiesManager.GridEnemiesManager.AllEnemiesEliminated += OnAllEnemiesEliminated;
        EnemiesManager.GridEnemiesManager.EnemyReachPlayer += OnEnemyReachPlayer;

        EndGame += EnemiesManager.OnEndGame;
    }

    public void OnGameOptionStart(object sender, EventArgs e)
    {
        StartNewGame();
    }
    
    public void OnPlayAgain(object sender, EventArgs e)
    {
        StartNewGame();
    }
    
    private void OnAllEnemiesEliminated(object sender, EventArgs e)
    {
        _round++;
        CreateEnemies();
        ConfigSubscribers();
        EnemiesManager.StartEnemies();
    }
    
    private void OnEnemyTakeDamage(object sender, EventArgs e)
    {
        Score += ((AbstractEnemy)sender).Points;
        
        if (Score >= _extraLifeIndex && Player.Lives < 6)
        {
            _extraLifeIndex += 1000;
            
            if (Player.Lives < 6)
            {
                Player.Lives++;
            }
        }
    }

    private void OnEnemyReachPlayer(object sender, EventArgs e)
    {
        EndGame.Invoke(this, new ScoreArg(Score));
    }
    
    private void OnPlayerDeath(object sender, EventArgs e)
    {
        EndGame.Invoke(this, new ScoreArg(Score));
    }
    
    public event EventHandler EndGame;
    
}