using System;
using System.Drawing;

namespace SpaceInvadersCapstone.ViewModels;

public class GameControlsViewModel : ViewModelBase
{
   public event EventHandler ExitToMainScreen;
   
   public void OnExitButtonClicked()
   {
      ExitToMainScreen.Invoke(this, EventArgs.Empty);
   }
}