﻿using System;
using ReactiveUI;

namespace SpaceInvadersCapstone.ViewModels;

public class MainWindowViewModel : ViewModelBase
{
    private ViewModelBase _contentViewModel;

    public ViewModelBase ContentViewModel
    {
        get => _contentViewModel;
        private set => this.RaiseAndSetIfChanged(ref _contentViewModel, value);
    }
    
    private StartMenuViewModel StartMenu { get; }
    
    private GameViewModel Game { get; }

    private GameOverViewModel GameOver { get; }
    
    private LeaderboardViewModel Leaderboard { get; }
    
    private GameControlsViewModel GameControls { get; }

    public MainWindowViewModel()
    {
        StartMenu = new StartMenuViewModel();
        Game = new GameViewModel();
        GameOver = new GameOverViewModel();
        Leaderboard = new LeaderboardViewModel();
        GameControls = new GameControlsViewModel();

        StartMenu.StartGameOption += OnGameOptionStart;
        StartMenu.StartGameOption += Game.OnGameOptionStart;
        StartMenu.LeaderboardOption += OnShowLeaderboard;
        StartMenu.GameControlsOption += OnGameControlsOption;

        Game.EndGame += GameOver.OnEndGame;
        Game.EndGame += OnEndGame;

        GameOver.PlayAgain += Game.OnPlayAgain;
        GameOver.PlayAgain += OnPlayAgain;
        GameOver.ExitToMainScreen += OnExitToMainScreen;

        Leaderboard.ExitToMainScreen += OnExitToMainScreen;

        GameControls.ExitToMainScreen += OnExitToMainScreen;

        _contentViewModel = StartMenu;
    }

    private void OnGameOptionStart(object sender, EventArgs e)
    {
        ContentViewModel = Game;
    }
    
    private void OnGameControlsOption(object sender, EventArgs e)
    {
        ContentViewModel = GameControls;
    }

    private void OnEndGame(object sender, EventArgs e)
    {
        ContentViewModel = GameOver;
    }
    
    private void OnPlayAgain(object sender, EventArgs e)
    {
        ContentViewModel = Game;
    }
    
    private void OnExitToMainScreen(object sender, EventArgs e)
    {
        ContentViewModel = StartMenu;
    }

    private void OnShowLeaderboard(object sender, EventArgs e)
    {
        Leaderboard.UpdateList();
        ContentViewModel = Leaderboard;
    }
}