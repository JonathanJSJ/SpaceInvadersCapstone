using System;
using System.Collections.ObjectModel;
using ReactiveUI;
using SpaceInvadersCapstone.Models;
using SpaceInvadersCapstone.Models.Utils;

namespace SpaceInvadersCapstone.ViewModels;

public class GameOverViewModel : ViewModelBase
{
    private int _score;
    public int Score
    {
        get => _score;
        set => this.RaiseAndSetIfChanged(ref _score, value);
    }

    public void OnEndGame(object sender, EventArgs score)
    {
        Score = (score as ScoreArg).Score;
    }

    public void OnExitButtonClicked()
    {
        ExitToMainScreen.Invoke(this, EventArgs.Empty);
    }

    public event EventHandler ExitToMainScreen;
    
    public void OnPlayButtonClicked()
    {
        PlayAgain.Invoke(this, EventArgs.Empty);
    }

    public event EventHandler PlayAgain;
    
    public void OnSaveButtonClicked(string name)
    {
        FileManager.SaveScore(new Score(name, Score));
    }
    
}