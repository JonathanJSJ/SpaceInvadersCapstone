using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Interactivity;
using SpaceInvadersCapstone.Models;
using SpaceInvadersCapstone.ViewModels;

namespace SpaceInvadersCapstone.Views;

public partial class GameView : UserControl
{
    public GameView()
    {
        InitializeComponent();

        Loaded += GameViewLoaded;
        KeyDown += KeyDownHandler;
    }

    private void GameViewLoaded(object? sender, RoutedEventArgs e)
    {
        //Canvas name
        GameScreen.Focus();
    }

    private void KeyDownHandler(object? sender, KeyEventArgs e)
    {
        var viewModel = DataContext as GameViewModel;

        if (sender != null && viewModel != null)
        {
            switch (e.Key)
            {
                case Key.Left:
                    viewModel.Player.Move(Player.Directions.Left);
                    break;
                case Key.Right:
                    viewModel.Player.Move(Player.Directions.Right);
                    break;
                case Key.Space:
                    viewModel.Player.Shot();
                    break;
                
            }
        }
    }
}