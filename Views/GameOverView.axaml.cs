using System;
using System.Linq;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Markup.Xaml;
using Avalonia.Markup.Xaml.MarkupExtensions;
using SpaceInvadersCapstone.ViewModels;

namespace SpaceInvadersCapstone.Views;

public partial class GameOverView : UserControl
{
    private Button saveButton;
    public GameOverView()
    {
        InitializeComponent();

        var exitButton = this.FindControl<Button>("ExitButton");
        exitButton.Click += OnExitButtonClicked;
        
        var playButton = this.FindControl<Button>("PlayButton");
        playButton.Click += OnPlayButtonClicked;
        
        saveButton = this.FindControl<Button>("SaveButton");
        saveButton.Click += OnSaveButtonClicked;
    }

    private void OnExitButtonClicked(object sender, EventArgs e)
    {
        var viewModel = DataContext as GameOverViewModel;
        viewModel.OnExitButtonClicked();
    }
    
    private void OnPlayButtonClicked(object sender, EventArgs e)
    {
        var viewModel = DataContext as GameOverViewModel;
        viewModel.OnPlayButtonClicked();
        
    }
    
    private void OnSaveButtonClicked(object sender, EventArgs e)
    {
        var nameBox = this.FindControl<TextBox>("NameBox");
        string scoreUserName = nameBox.Text;
        
        if (scoreUserName.All(char.IsLetter))
        {
            SaveButton.IsEnabled = false;
            nameBox.IsEnabled = false;
        
            var viewModel = DataContext as GameOverViewModel;
            viewModel.OnSaveButtonClicked(scoreUserName);

            SaveButton.Flyout.Hide();
        }
    }
    
}