using Avalonia.Controls;
using SpaceInvadersCapstone.Models.Utils;

namespace SpaceInvadersCapstone.Views;

public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
        Width = Constants.ScreenWidth;
        Height = Constants.ScreenHeight;
    }
    
}