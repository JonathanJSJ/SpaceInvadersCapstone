# Programming 3 Capstone - Jala University

### Warning! 
This project may encounter some issues when launching due to the LibVLCSharp library, which is used in the [SoundEffect.cs](Models%2FUtils%2FSoundEffect.cs) class. Commenting out this class method may serve as a workaround to play this project.

### Saving scores
When the first score is saved, a new directory named "SpaceInvadersCapstone" is created in the user's home folder. Inside this directory, a new CSV file will also be created. Do not modify its path or the CSV file saved within, or a new scoreboard will be created.

# Author
### Jonathan Silva